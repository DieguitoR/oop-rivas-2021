import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {
		

		// Benutzereingaben lesen
		
		String artikel = liesString ("was m�chten Sie bestellen?");
		
		 int anzahl =  liesInt ("Geben Sie die Anzahl ein:");
		 
		 double nettopreis = liesDouble ("Geben Sie den Nettopreis ein:");
		 
		 double porzent = liesDouble ("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		 
		// Verarbeiten
		 
		 double nettogesamtpreis =  berechneGesamtnettopreis (anzahl, nettopreis);
		 
		 double total =  berechneGesamtbruttopreis (nettogesamtpreis, porzent);
		 
		// Ausgeben
		 System.out.println("Vielen dank f�r ihren Einkauf !!!");
		 rechungausgeben(artikel, anzahl, nettogesamtpreis, total, porzent);


		}

	public  static String liesString (String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		String artikel = myScanner.next();
		return artikel;  }

	public static int liesInt ( String text){
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		int anzahl = myScanner.nextInt();
		return anzahl;   }
	
	public static double liesDouble (String text){
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		double preis = myScanner.nextDouble();
		return preis;  }
	
	public static double berechneGesamtnettopreis (int anzahl, double nettopreis){
		double totalpreis = anzahl * nettopreis;
		return totalpreis;   }
	
	public static double berechneGesamtbruttopreis (double nettogesamtpreis, double mwst){
		double summeBrutto = nettogesamtpreis * (1 + mwst / 100);
		return summeBrutto;  }
	
	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis,double mwst){
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
}
