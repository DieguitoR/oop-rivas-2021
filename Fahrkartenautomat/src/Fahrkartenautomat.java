﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
      // Scanner tastatur = new Scanner(System.in);
      
      
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       
       double zuZahlenderWert;
       double zuZahlenderBetrag; 
       int ticketsAnzahl;
       /*
        System.out.print("Wie viele Fahrkarten? ");
       ticketsAnzahl = tastatur.nextInt();
       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderWert = tastatur.nextDouble();
      zuZahlenderBetrag = (zuZahlenderWert*ticketsAnzahl);
      */
       
     
       zuZahlenderBetrag = fahrkartenbestellungErfassen();
      
      // Geldeinwurf
       eingezahlterGesamtbetrag = fahrkartenBezahlen (zuZahlenderBetrag); 
      
      // Fahrscheinausgabe
      fahrkartenAusgeben ();
      
      // Rückgeldberechnung und -Ausgabe
      
      rueckgeldAusgeben (eingezahlterGesamtbetrag, zuZahlenderBetrag );
       
     
 
     
       /* / -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.println("Anzahl der Tickets: " + ticketsAnzahl );
    	   System.out.printf("Noch zu zahlen: " +  "%.2f Euro %n ",(zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }
   
     
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");
       
      

     
       // -------------------------------
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rückgabebetrag > 0.0)
       {
    	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
       
    		  */
    }
    
    
    	public static double fahrkartenbestellungErfassen () {
    		Scanner tastatur = new Scanner(System.in);
 
    		System.out.print("Wie viele Fahrkarten? ");
    		int  ticketsAnzahl = tastatur.nextInt();
       
    		System.out.print("Zu zahlender Betrag (EURO): ");
    		double zuZahlenderWert = tastatur.nextDouble();
 
    		double zuZahlenderBetrag = (zuZahlenderWert*ticketsAnzahl);
    		return zuZahlenderBetrag;
    		
    	
       } 
    
    	public static double fahrkartenBezahlen (double preis) {
    		Scanner tastatur = new Scanner(System.in);
    		double  eingezahlterGesamtbetrag = 0.0;
    	 	while(eingezahlterGesamtbetrag < preis ) {
    	 			System.out.printf("Noch zu zahlen: " +  "%.2f Euro %n ",(preis - eingezahlterGesamtbetrag));
    	 			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	 			double eingeworfeneMünze = tastatur.nextDouble();
    	 			eingezahlterGesamtbetrag += eingeworfeneMünze;}
    	 	return eingezahlterGesamtbetrag;
    	}
    	
    	public static void fahrkartenAusgeben () {
    	      System.out.println("\nFahrschein wird ausgegeben");
    	       for (int i = 0; i < 8; i++)
    	       {
    	          System.out.print("=");
    	          try {
    				Thread.sleep(250);
    			} catch (InterruptedException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    	       }
    	       System.out.println("\n\n");
    	}
    	public static void rueckgeldAusgeben (double eingezahlterGesamtbetrag, double zuZahlenderBetrag ) {
    		 double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
    	       if(rückgabebetrag > 0.0)
    	       {
    	    	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
    	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

    	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
    	           {
    	        	  System.out.println("2 EURO");
    		          rückgabebetrag -= 2.0;
    	           }
    	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
    	           {
    	        	  System.out.println("1 EURO");
    		          rückgabebetrag -= 1.0;
    	           }
    	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
    	           {
    	        	  System.out.println("50 CENT");
    		          rückgabebetrag -= 0.5;
    	           }
    	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
    	           {
    	        	  System.out.println("20 CENT");
    	 	          rückgabebetrag -= 0.2;
    	           }
    	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
    	           {
    	        	  System.out.println("10 CENT");
    		          rückgabebetrag -= 0.1;
    	           }
    	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
    	           {
    	        	  System.out.println("5 CENT");
    	 	          rückgabebetrag -= 0.05;
    	           }
    	       }

    	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
    	                          "vor Fahrtantritt entwerten zu lassen!\n"+
    	                          "Wir wünschen Ihnen eine gute Fahrt.");
    	}
    
}